const config = require('./config.js')
const restify = require('restify')
const builder = require('botbuilder')
const recast = require('recastai')

const connector = new builder.ChatConnector({
    appId: config.appId,
    appPassword: config.appPassword
})

const bot = new builder.UniversalBot(connector)
const recastClient = new recast.Client(config.recast)

const INTENTS = {
    greetings: getGreetings,
    infohero: getInfoHero
}

bot.dialog('/', (session) => {
    recastClient.textRequest(session.message.text)
        .then(res => {
            const intent = res.intent()
            const entity = rest.get('hero')
            session.send(`Intent: ${intent.slug}`)
            session.send(`Entity: ${entity.name}`)
            if (intent) {
                session.send(INTENTS[intent.slug](entity))
            }
        })
        .catch(() => session.send('I need some sleed right now... Talk to me later!'))
})

const server = restify.createServer()
server.listen(8080)
server.post('/', connector.listen())
